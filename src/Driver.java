import java.util.Map;

public class Driver {

	public static void main(String[] args) {
		Map<String, Integer> map = new HashTable<String, Integer>();
		System.out.println(map);
		
		map.put("Apples", 5);
		System.out.println(map);
		
		map.put("Apples", 6);
		System.out.println(map);

		map.put("Bananas", 10);
		map.put("Kiwi", 1);
		map.put("Mango", 2);
		map.put("Pineapple", 3);
		map.put("Jack Fruit", 6);
		map.put("Peach", 2);
		System.out.println(map);
		
		map.put("Plumb", 4);
		System.out.println(map);
		
		map.remove("Bananas");
		System.out.println(map);
		
		System.out.println(map.get("Bananas"));
		
		System.out.println(map.get("Pineapple"));
	}

}
