import java.util.Map;

public class HashTableEntry<K,V> implements Map.Entry<K, V> {
	
	private K key;
	private V value;
	
	public HashTableEntry(K key, V value) {
		this.key = key;
		this.value = value;
	}
	
	public K getKey() {
		return key;
	}
	
	public V getValue() {
		return value;
	}
	
	public void setKey(K key) {
		this.key = key;
	}
	
	public V setValue(V value) {
		V oldValue =  this.value;
		this.value = value;
		
		return oldValue;
	}
	
}
