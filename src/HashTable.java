import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class HashTable<K, V> implements Map<K, V> {

	private final double LOAD_FACTOR_THRESHOLD = 0.75;
	
	private List<Entry<K,V>>[] entries;
	private int size;
	
	public HashTable() {
		this(10);
	}
	
	public HashTable(int initialCapacity) {
		entries = (LinkedList<Entry<K, V>>[]) new LinkedList[initialCapacity];
		size = 0;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public boolean containsKey(Object key) {
		
		if (key == null) {
			throw new NullPointerException("Key is null");
		}
		
		int index = key.hashCode() % entries.length;
		
		if (entries[index] == null) {
			return false;
		} else {
			for (Entry<K, V> entry : entries[index]) {
				if (entry.getKey().equals(key)) {
					return true;
				}
			}
		}
		
		return false;
	}

	@Override
	public boolean containsValue(Object value) {
		
		for (int i = 0; i < entries.length; i++) {
			
			if (entries[i] != null) {
				
				for (Entry<K, V> entry : entries[i]) {
					
					if (entry.getValue() != null && entry.getValue().equals(value)) {
						return true;
					} else if (entry.getValue() == null && value == null) {
						return true;
					}
					
				}
				
			}
			
		}
		
		return false;
	}

	@Override
	public V get(Object key) {
		
		if (key == null) {
			throw new NullPointerException("Non-null key required");
		}
		
		int index = key.hashCode() % entries.length;
		
		if (entries[index] == null) {
			return null;
		} else {
			List<Entry<K, V>> entryList = entries[index];
			
			for (int i = 0; i < entryList.size(); i++) {
				
				Entry<K, V> entry = entryList.get(i);
				
				if (entry.getKey().equals(key)) {
					return entry.getValue();
				}
				
			}
		}
		
		return null;
	}

	@Override
	public V put(K key, V value) {
		V valueReplaced = null;
		
		int hashcode = key.hashCode();
		int index = hashcode % entries.length;
		
		// Case 1: This slot is seeing a value for the first time.
		if (entries[index] == null) {
			
			List<Entry<K, V>> entryList = new LinkedList<Entry<K, V>>();
			entryList.add(new HashTableEntry<K, V>(key, value));
			
			entries[index] = entryList;
			
		} else { // Case 2: This slot has seen a value already
			
			List<Entry<K, V>> entryList = entries[index];
			
			// Look for the same key, if there is an entry with the same key,
			// we want to replace the value.
			for (int i = 0; i < entryList.size(); i++) {
				Entry<K,V> currentEntry = entryList.get(i);
				
				if (currentEntry.getKey().equals(key)) {
					valueReplaced = currentEntry.getValue();
					currentEntry.setValue(value);
				}
			}
			
			// If the same key was not found, we want to add a new entry to the list
			if (valueReplaced == null) {
				entryList.add(new HashTableEntry<K, V>(key, value));
			}
		}
		
		if (valueReplaced == null) {
			size++;
		}
		
		double currentLoadFactor = ((double) size) / entries.length;
		if (currentLoadFactor > LOAD_FACTOR_THRESHOLD) {
			resize();
		}
		
		return valueReplaced;
	}
	
	private void resize() {
		List<Entry<K, V>>[] oldEntries = entries;
		entries = (LinkedList<Entry<K, V>>[]) new LinkedList[entries.length * 2];
		
		// Setting size to 0 because when calling the 'put(...)' method below, the size will be
		// incremented.
		size = 0;
		
		List<Entry<K, V>> allEntriesList = new LinkedList<Entry<K, V>>();
		
		for (List<Entry<K, V>> entryList : oldEntries) {
			if (entryList != null) {
				allEntriesList.addAll(entryList);
			}
		}
		
		for (Entry<K, V> entry : allEntriesList) {
			put(entry.getKey(), entry.getValue());
		}
	}

	@Override
	public V remove(Object key) {
		
		if (key == null) {
			throw new NullPointerException("Expected a non-null key.");
		}
		
		V valueRemoved = null;
		
		int index = key.hashCode() % entries.length;
		List<Entry<K, V>> entryList = entries[index];
		
		if (entryList != null) {
			for (Entry<K, V> entry : entryList) {
				if (key.equals(entry.getKey())) {
					valueRemoved = entry.getValue();
					entryList.remove(entry);
					break;
				}
			}
		}
		
		if (valueRemoved != null) {
			size--;
		}
		
		return valueRemoved;
	}

	@Override
	public void putAll(Map<? extends K, ? extends V> m) {
//		Collection<K> keySet = (Collection<K>) m.keySet();
//		Collection<V> valueSet = (Collection<V>) m.values();
//		
//		Collection<?> entrySet = m.entrySet();
//		for (Object entry : entrySet) {
//			Entr
//		}
		// TODO
	}

	@Override
	public void clear() {
		entries = (LinkedList<Entry<K, V>>[]) new LinkedList[entries.length];
		size = 0;
	}

	@Override
	public Set<K> keySet() {
		Set<K> keySet = new HashSet<K>();
		
		for (int i = 0; i < entries.length; i++) {
			
			List<Entry<K, V>> entryList = entries[i];
			
			if (entryList != null) {
				for (Entry<K, V> entry : entryList) {
					keySet.add(entry.getKey());
				}	
			}
			
		}
		
		return keySet;
	}

	@Override
	public Collection<V> values() {
		Collection<V> values = new LinkedList<V>();
		
		for (int i = 0; i < entries.length; i++) {
			
			if (entries[i] == null) {
				continue;
			}
			
			for (Entry<K, V> entry : entries[i]) {
				values.add(entry.getValue());
			}
			
		}
		
		return values;
	}

	@Override
	public Set<Entry<K, V>> entrySet() {
		Set<Entry<K, V>> entrySet = new HashSet<Entry<K, V>>();
		
		for (int i = 0; i < entries.length; i++) {
			
			if (entries[i] == null) {
				continue;
			}
			
			entrySet.addAll(entries[i]);
		}
		
		return entrySet;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		for (int i = 0; i < entries.length; i++) {
			
			if (entries[i] == null) {
				continue;
			}
			
			for (Entry<K, V> entry : entries[i]) {
				builder.append(String.format("Key: %s - Value: %s\n", entry.getKey(), entry.getValue()));
			}
		}
		
		builder.append(String.format("Size: %d - Capacity: %d - Load Factor: %.2f\n", size, entries.length, ((double) size) / entries.length));
		
		return builder.toString();
	}
}
